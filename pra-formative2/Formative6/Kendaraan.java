public class Kendaraan {// Membuat kelas Kendaraan dengan atribut jumlahRoda
                        // dan dengan method display() yang kemudian
                        // dapat diwariskan ke kelas lainnya.
                        // Sehingga kelas Kendaraan disebut juga sebagai
                        // super class atau parent class
    int jumlahRoda = 2;
    
    void display(){
        System.out.println("Kendaraan dengan jumlah roda: " + this.jumlahRoda);
    }
}
