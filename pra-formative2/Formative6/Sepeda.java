public class Sepeda extends Kendaraan { // Membuat kelas Sepeda dan mewarisi atribut dan method dari
                                        // Class Kendaraan
    String berjalan = "dengan Engkol (Tanpa Mesin).";

    // Overriding Methods
    // method ini yang pertama kali akan dieksekusi jika kita memanggil
    // fungsi diplay pada objek Sepeda dari main method
    // jika di dalam Objek Motor tidak terdapat method display, baru
    // dia akan menampilkan method display dari Class Kendaraan
    void display(){
        System.out.println("==== SEPEDA ====");
        System.out.println("Sepeda memiliki jumlah roda: " + super.jumlahRoda);// keyword super untuk mengambil nilai 2
                                                                                // superclass Kendaraan agar bisa diakses di
                                                                                // child class Motor
        System.out.println("Berjalan dengan: "+this.berjalan); // keyword this mengambil nilai 
                                                                // dari kelas ini sendiri
    }
}
