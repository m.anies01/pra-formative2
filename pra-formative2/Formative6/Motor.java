public class Motor extends Kendaraan { // Membuat kelas Motor dan mewarisi atribut dan method dari
                                        // Class Kendaraan
    String berjalan = "Menggunakan Mesin"; // memberikan nilai pada variabel berjalan dengan
                                        // string menggunakan mesin

    // Overriding methods
    // method ini yang pertama kali akan dieksekusi jika kita memanggil
    // fungsi diplay pada objek motor dari main method
    // jika di dalam Objek Motor tidak terdapat method display, baru
    // dia akan menampilkan method display dari Class Kendaraan
    void display(){
        System.out.println("==== MOTOR ====");
        System.out.println("Motor memiliki jumlah roda: " + super.jumlahRoda); // keyword super untuk mengambil nilai 2
                                                                                // superclass Kendaraan agar bisa diakses di
                                                                                // child class Motor
        System.out.println("Berjalan dengan: "+this.berjalan);
    }
}
